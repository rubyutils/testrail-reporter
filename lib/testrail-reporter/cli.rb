require 'optparse'
require 'ostruct'
require_relative 'reporter_manager'
require_relative 'version'

module TestRailReporter
  class CLI
    def initialize
      @options = {}
      @options[:config] = ''
      @options[:log_path] = ''
      @options[:console_report] = false
      @options[:testrail_report] = false
    end

    def validate_config?(config)
      !config.empty?
    end

    def validate_log_path?(log_path)
      !log_path.empty?
    end

    def parse
      OptionParser.new do |opts|
        opts.on('-v', '--version') do
          puts TestRailReporter::VERSION
          exit
        end

        opts.on('-h', '--help') do
          puts opts
          exit
        end

        opts.on('-f', '--config CONFIG', 'data config file path') do |config|
          @options[:config] = config
        end

        opts.on('-l', '--log-path CONFIG', 'log path filter, can use glob pattern') do |log_path|
          @options[:log_path] = File.expand_path(log_path)
        end

        opts.on('-c', '--console', 'print report to the console') do
          @options[:console_report] = true
        end

        opts.on('-t', '--testrail', 'send testing results to TestRail') do
          @options[:testrail_report] = true
        end
      end.parse!

      if @options[:console_report] && !validate_log_path?(@options[:log_path])
        raise 'Missing log path input, -l|--log-path'
      end

      if @options[:testrail_report] && !validate_config?(@options[:config])
        raise 'Config path can not be empty'
      end

      ReporterManager.new(@options).report
    end
  end # end of class
end # end of module
