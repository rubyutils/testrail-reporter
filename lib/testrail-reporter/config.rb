require 'erb'

module TestRailReporter
  module Config
    def self.get(config_path)
      YAML.load(render(config_path))
    end

    def self.render(config_path)
      absolute_path = File.expand_path(config_path)
      ERB.new(IO.read(absolute_path))
      ERB.result
    end
  end
end
