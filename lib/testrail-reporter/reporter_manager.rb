require_relative 'console_reporter'

module TestRailReporter
  class ReporterManager
    def initialize(options)
      @options = options
    end

    def banner(message)
      puts '-----------------------------------------------------------------'
      puts message
      puts '-----------------------------------------------------------------'
    end

    def report
      reporter = nil

      if @options[:console_report]
        reporter = ConsoleReporter.new(@options)
      end

      if @options[:testrail_report]
        reporter = JUnitReporter.new(@options)
      end

      reporter.report if reporter
    end
  end
end
