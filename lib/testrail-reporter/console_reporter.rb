require_relative 'result_set'

module TestRailReporter
  class ConsoleReporter
    def initialize(options)
      @log_path = options[:log_path]
    end

    def report
      ResultSet.new(@log_path).print_results
    end
  end
end
