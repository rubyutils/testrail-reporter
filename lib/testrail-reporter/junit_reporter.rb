require_relative 'config'

module TestRailReporter
  class JUnitReporter
    def initialize(options)
      @options = options
    end

    def report
      config_data = Config.get(@options[:config])
      auth = get_auth(config_data['auth'])

      if config_data.key?('data')
        config_data['data'].each do |key, entry|
          results = TestRailJunitReporter.new(entry, auth)
          results.report_results
        end
      end
    end

    def get_auth(auth)
      required_auth = %w(TESTRAIL_URL TESTRAIL_USERNAME TESTRAIL_PASSWORD)
      copy_auth = {}

      auth.each do |k, v|
        copy_auth[k] = v
      end

      required_auth do |k|
        copy_auth[k] = ENV[k]
      end

      validate_auth(copy_auth, required_auth)
    end

    def validate_auth(copy_auth, required_auth)
      required_auth do |k|
        if !copy_auth.key?(k) || copy_auth[k].empty?
          raise "Missing required: #{k}"
        end
      end
      copy_auth
    end
  end
end
