require 'testrail-ruby'
require_relative 'result_set.rb'

# This tool converts local JUnit XML test reports into a TestRail test plan run
# Required config_data to report results to TestRail:
#  project_id: 2 --> this is the TestRail project ID
#  suite_id: 48 --> this is the suite_id for the test cases we'll use
#  template_id: 4 --> required for test cases creation, refer to the TestRail
#                     customizations admin
#  type_id: 3 --> required for test case creation,  refer to the TestRail
#                 customizations admin
#  priority_id: 2 --> required for test case creation, refer to the TestRail
#                     customizations admin
#  testrail_url: 'https://testrail.example.com/', --> url to access TestRail
#  tests_dir_path: 'logs/test-reports/*-ga-*' --> glob path for junit xml files
module TestRailReporter
  class TestRailJunitReporter
    def initialize(config_data, auth)
      config_data.each do |key, value|
        instance_variable_set("@#{key}", value)
      end

      @client = TestRail::APIClient.new(auth['TESTRAIL_URL'])
      @client.user = auth['TESTRAIL_USERNAME']
      @client.password = auth['TESTRAIL_PASSWORD']
    end

    # Read local junit xml files and create a data structure of:
    # [{testrail_case_id, testrail_section_id, classname, name,
    #   failed, skipped, error_msg, failure_msg}]
    def read_junit_results(glob_path = @tests_dir_path)
      result_set_tmp = ResultSet.new(glob_path)
      result_set_tmp.print_stats
      @result_set = result_set_tmp.results.compact
    end

    def report_results
      read_junit_results(@tests_dir_path)
      # Create a test run
      test_run = @client.add_run(@project_id,
                                 name:        @test_plan_name,
                                 description: @test_run_description,
                                 suite_id:    @suite_id,
                                 include_all: true)
      run_id = test_run['id']

      # Create the test sections and test cases if they do not exist, then add the
      # test case result to the test run
      @result_set.each do |result|
        # Create section if it doesn't exist
        sections = @client.get_sections(@project_id, suite_id: @suite_id)
        matched_section = sections.find do |section|
          result[:classname] == section['name']
        end
        section_id = if matched_section
                       matched_section['id']
                     else
                       rslt = @client.add_section(@project_id,
                                                  suite_id: @suite_id,
                                                  name:     result[:classname])
                       rslt['id']
                     end

        # Create Test Case if it doesn't exist
        cases = @client.get_cases(@project_id,
                                  suite_id:   @suite_id,
                                  section_id: section_id)
        matched_case = cases.find do |tcase|
          result[:name] == tcase['title']
        end
        case_id = if matched_case
                    matched_case['id']
                  else
                    rslt = @client.add_case(section_id,
                                            title:       result[:name],
                                            template_id: @template_id,
                                            type_id:     @type_id,
                                            priority_id: @priority_id)
                    rslt['id']
                  end

        # Update our Test Plan for this test case
        test_result = 1 # pass
        test_result = 5 if result[:failed] || result[:error] # fail
        test_result = 9 if result[:skipped] # skip
        error_msg = "#{result[:error_msg]} #{result[:failure_msg]}"
        @client.add_result_for_case(run_id,
                                    case_id,
                                    status_id: test_result,
                                    comment:   error_msg)
      end

      # Close the Test Run
      @client.close_run(run_id)
    end
  end # end of class
end # end of module
