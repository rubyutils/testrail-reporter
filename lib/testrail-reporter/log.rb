require 'logger'
require 'rainbow'

unless defined?($log)
  $log = Logger.new(STDOUT)
  LOGLEVEL = ENV['LOGLEVEL'] ? ENV['LOGLEVEL'].upcase : 'INFO'

  log_meta = {
    'DEBUG' => {
      :color => lambda {|msg| Rainbow(msg).magenta }
    },
    'INFO' => {
      :color => lambda {|msg| Rainbow(msg).cyan }
    },
    'WARN' => {
      :color => lambda {|msg| Rainbow(msg).yellow }
    },
    'ERROR' => {
      :color => lambda {|msg| Rainbow(msg).red }
    },
    'FATAL' => {
      :color => lambda {|msg| Rainbow(msg).red.bold }
    }
  }

  if log_meta.key?(LOGLEVEL)
    $log.level = Logger.const_get(LOGLEVEL.to_sym)

    $log.formatter = Proc.new do |severity, datetime, progname, msg|
      date_format = datetime.strftime('%Y-%m-%d %H:%M:%S')
      formatted = "[#{date_format}] #{severity} #{progname}: #{msg}\n"

      log_color = log_meta[severity][:color]
      formatted = log_color.call(formatted)
      formatted
    end
  else
    raise "Undefined LOGLEVEL #{LOGLEVEL}"
  end
end
