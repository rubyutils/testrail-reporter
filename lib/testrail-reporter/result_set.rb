require 'nokogiri'
require_relative 'log'

# Parses directories of junit xml test reports
module TestRailReporter
  class ResultSet
    attr_accessor :results

    def initialize(path)
      @results = read_results(path)
    end

    def read_results(path)
      xml_files = get_junit_xml_files(path)
      xml_files.map do |junit_report|
        parse_xml(junit_report)
      end.reduce(:concat)
    end

    def print_results
      @results.each { |result| print_test_result(result) }
    end

    def get_junit_xml_files(glob_path)
      Dir.glob("#{glob_path}/*.xml")
    end

    def parse_xml(filename)
      doc = File.open(filename) { |f| Nokogiri::XML(f) }

      testsuites = doc.xpath('testsuite')
      testcases = testsuites.xpath('testcase')
      testcases.map do |testcase|
        error_msg = if testcase.xpath('error').any?
                      testcase.xpath('error')
                    else
                      ''
                    end
        failure_msg = if testcase.xpath('failure').any?
                        testcase.xpath('failure')
                      else
                        ''
                      end
        { classname:   testcase.attributes['classname'].value,
          name:        testcase.attributes['name'].value.gsub(/^test_\d+_|test_/, '').strip,
          failed:      testcase.xpath('failure').any? || testcase.xpath('error').any?,
          skipped:     testcase.xpath('skipped').any?,
          error_msg:   error_msg,
          failure_msg: failure_msg }
      end
    end

    def print_test_result(rslt)
      if rslt[:failed]
        $log.error "#{rslt[:classname]}: #{rslt[:name]}: FAIL"
      elsif rslt[:skipped]
        $log.warn "#{rslt[:classname]}: #{rslt[:name]}: SKIPPED"
      else
        $log.info "#{rslt[:classname]}: #{rslt[:name]}: PASS"
      end
    end

    def print_stats
      pass = @results.compact.count { |rslt| !(rslt[:failed] || rslt[:skipped]) }
      fails = @results.compact.count { |rslt| rslt[:failed] }
      skips = @results.compact.count { |rslt| rslt[:skipped] }
      $log.info "Total tests: #{@results.size}"
      $log.info "Total passed: #{pass}"
      $log.info "Total failed: #{fails}"
      $log.info "Total skipped: #{skips}"
    end
  end # end of class
end # end of module
