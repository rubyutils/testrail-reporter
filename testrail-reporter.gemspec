# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)

require 'testrail-reporter/version'

Gem::Specification.new do |spec|
  spec.name             = 'testrail-reporter'
  spec.version          = TestRailReporter::VERSION
  spec.authors          = ['Oliver Yang']
  spec.email            = 'oyang@oblong.com'

  spec.summary          = 'JUnit file render into report'
  spec.description      = '''A tool to render JUnit file into console and TestRail report'''
  spec.homepage         = 'https://gitlab.com/rubyutils/testrail-reporter'
  spec.license          = 'MIT'

  spec.require_paths    = ['lib']
  spec.files            = Dir['lib/**/*'] + ['Gemfile', 'README.md', 'LICENSE']
  spec.test_files       = Dir['spec/**/*']
  spec.executables << 'testrail-reporter'

  spec.required_ruby_version = '>= 2.3.0'

  spec.add_runtime_dependency 'rainbow', '~> 2.2', '>= 2.2.2'
  spec.add_runtime_dependency 'nokogiri', '~> 1.8', '>= 1.8.0'
  spec.add_runtime_dependency 'testrail-ruby', '~> 0.1.02'

  spec.add_development_dependency 'bundler', '~> 1.14', '>= 1.14.6'
  spec.add_development_dependency 'rake', '~> 12.0', '>= 12.0.0'
end
