##### A tool suit to render JUnit file into TestRail report

```shell
Usage: testrail-reporter [options]
    -v, --version
    -h, --help
    -f, --config CONFIG              data config file path
    -l, --log-path CONFIG            log path filter, can use glob pattern
    -c, --console                    print report to the console
    -t, --testrail                   send testing results to TestRail
```
